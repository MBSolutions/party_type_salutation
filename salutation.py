# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from string import Template
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool


class Salutation(ModelSQL, ModelView):
    'Salutation'
    _name = 'party.salutation'
    _description = __doc__

    default = fields.Boolean('Default Salutation')
    target = fields.Selection([
        ('organization', 'Organization'),
        ('female', 'Female Person'),
        ('male', 'Male Person'),
        ('unknown', 'Person (unknown gender)'),
    ], 'Target', required=True)
    salutation_template = fields.Char('Salutation Template', required=True,
        translate=True, loading='lazy')

    def __init__(self):
        super(Salutation, self).__init__()
        self._constraints += [
            ('check_unique_default', 'unique_default'),
        ]
        self._error_messages.update({
            'missing_default': 'Default salutation not found!',
            'unique_default': 'There can be only one default salutation!',
        })

    def check_unique_default(self, ids):
        args = [('default', '=', True)]
        default_ids = self.search(args)
        res = True
        if len(default_ids) > 1:
            res = False
        return res

    def get_salutation(self, party=False):
        party_obj = Pool().get('party.party')

        target = 'organization'
        if party:
            if isinstance(party, (int, long)):
                party = party_obj.browse(party)
            if party.party_type == 'person':
                if party.gender:
                    target = party.gender
                else:
                    target = 'unknown'
        args = [
            ('target', '=', target),
        ]
        salrec = self.search_read(args,
            fields_names=['salutation_template'], limit=1)
        if not salrec:
            args = [
                ('default', '=', 'True'),
            ]
            salrec = self.search_read(args,
                fields_names=['salutation_template'], limit=1)
            if not salrec:
                self.raise_user_error('missing_default')
        res = ''
        if salrec:
            mapping = self._get_mapping(party)
            template = Template(salrec['salutation_template'])
            res = template.substitute(mapping)
        return res

    def _get_mapping(self, party):
        res = {
            'full_name': '',
            'first_name': '',
            'last_name': '',
        }
        if party:
            res['full_name'] = party.full_name
            res['first_name'] = party.first_name
            res['last_name'] = party.name
        return res

Salutation()
