# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool


class Party(ModelSQL, ModelView):
    _name = 'party.party'

    salutation = fields.Function(fields.Char('Salutation'), 'get_salutation')

    def get_salutation(self, ids, name):
        salutation_obj = Pool().get('party.salutation')
        res = {}
        for party in self.browse(ids):
            res[party.id] = salutation_obj.get_salutation(party=party)
        return res

Party()
